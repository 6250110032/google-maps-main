import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late GoogleMapController mapController;

  final LatLng _center = const LatLng(7.684211725194319, 99.31323259702698);

  Future<void> _onMapCreate(GoogleMapController mapController) async {
    mapController = mapController;
    String value = await DefaultAssetBundle.of(context)
        .loadString('assets/map_style.json');
    mapController.setMapStyle(value);
  }

  Set<Marker> _createMarker() {
    return {
      Marker(
        markerId: MarkerId("marker_1"),
        position: LatLng(7.518676899166074, 99.57879075393838),
        infoWindow: InfoWindow(
            title: 'PSU Trang',
            snippet: "มหาวิทยาลัยสงขลานครินทร์ วิทยาเขตตรัง"),
        icon: BitmapDescriptor.defaultMarker,
      ),
      Marker(
        markerId: MarkerId("marker_2"),
        position: LatLng(5.776822316208723, 101.06166246715149),
        infoWindow: InfoWindow(title: 'เบตง', snippet: "เบตง"),
        rotation: 90,
        icon: BitmapDescriptor.defaultMarker,
      ),
    };
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Maps Sample App'),
          backgroundColor: Colors.green[700],
        ),
        body: GoogleMap(
          myLocationEnabled: true,
          mapToolbarEnabled: true,
          onMapCreated: _onMapCreate,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 15.0,
          ),
          markers: _createMarker(),
        ),
      ),
    );
  }
}
